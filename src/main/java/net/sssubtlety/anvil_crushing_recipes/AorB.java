package net.sssubtlety.anvil_crushing_recipes;

import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityType;

public class AorB <A, B> {
    public final A a;
    public final B b;
//    private final Class<?> aClass;
//    private final Class<?> bClass;
    public final boolean isA;

    private AorB(Object aOrB, boolean isA) {
//        this.aOrB = aOrB;
        this.isA = isA;
        if (isA) {
            this.a = (A)aOrB;
            this.b = null;
        } else {
            this.a = null;
            this.b = (B)aOrB;
        }
    }

    public static <A, B> AorB<A,B> ofA(A a) {
        return new AorB<>(a, true);
    }

    public static <A, B> AorB<A,B> ofB(B b) {
        return new AorB<>(b, false);
    }

    public Object get() {
        return this.isA ? this.a : this.b;
    }
}
