package net.sssubtlety.anvil_crushing_recipes.mixin;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(ItemPlacementContext.class)
public interface ItemPlacementContextAccessor {
    @Invoker(value = "<init>")
    static ItemPlacementContext createItemPlacementContext(World world, PlayerEntity playerEntity, Hand hand, ItemStack itemStack, BlockHitResult blockHitResult) {
        throw new IllegalStateException("ItemPlacementContextAccessor's dummy constructor called. ");
    }
}
