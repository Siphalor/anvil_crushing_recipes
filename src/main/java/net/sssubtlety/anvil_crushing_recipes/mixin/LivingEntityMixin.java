package net.sssubtlety.anvil_crushing_recipes.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.world.World;
import net.sssubtlety.anvil_crushing_recipes.mixin_helpers.LivingEntityMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(LivingEntity.class)
public abstract class LivingEntityMixin extends Entity implements LivingEntityMixinAccessor {
    private boolean dropLoot;
    private static final float INSTANT_KILL_DAMAGE = 3.4028235E38F;

    @Shadow public abstract boolean damage(DamageSource source, float amount);

    @Shadow protected abstract void drop(DamageSource source);

//    @Shadow public abstract boolean damage(DamageSource source, float amount);
//    @Shadow protected boolean dead;
//    @Shadow protected int scoreAmount;
//
//
//    @Shadow public abstract LivingEntity getPrimeAdversary();
//    @Shadow protected abstract void onKilledBy(LivingEntity livingEntity);
////    @Shadow public abstract DataTracker getDamageTracker();
//    @Shadow public abstract void wakeUp();
//    @Shadow public abstract boolean isSleeping();

    public LivingEntityMixin(EntityType<?> type, World world) {
        super(type, world);
        throw new IllegalStateException("LivingEntityMixin's dummy constructor called. ");
    }

    @Inject(method = "<init>", at = @At("TAIL"))
    private void postConstruction(CallbackInfo ci) {
        this.dropLoot = true;
    }

    @Override
    public void customKill(DamageSource source, Boolean dropLoot) {
        this.dropLoot = dropLoot;
        this.damage(source, INSTANT_KILL_DAMAGE);
        this.dropLoot = true;
    }

    @Redirect(method = "onDeath", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/LivingEntity;drop(Lnet/minecraft/entity/damage/DamageSource;)V"))
    private void conditionallyDrop(LivingEntity livingEntity, DamageSource source) {
        if (this.dropLoot) this.drop(source);
        this.dropLoot = true;
    }

//    private void onDeathCodeDupeNoLoot(DamageSource source) {
//        if (!this.removed && !this.dead) {
//            Entity entity = source.getAttacker();
//            LivingEntity livingEntity = this.getPrimeAdversary();
//            if (this.scoreAmount >= 0 && livingEntity != null) {
//                livingEntity.updateKilledAdvancementCriterion(this, this.scoreAmount, source);
//            }
//
//            if (this.isSleeping()) {
//                this.wakeUp();
//            }
//
//            this.dead = true;
//            ((LivingEntity)(Object)this).getDamageTracker().update();
//            if (this.world instanceof ServerWorld) {
//                if (entity != null) {
//                    entity.onKilledOther((ServerWorld)this.world, (LivingEntity) (Object)this);
//                }
//
//                this.onKilledBy(livingEntity);
//            }
//
//            this.world.sendEntityStatus(this, (byte)3);
//            this.setPose(EntityPose.DYING);
//        }
//    }
}
