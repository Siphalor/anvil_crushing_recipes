package net.sssubtlety.anvil_crushing_recipes.mixin;

import net.minecraft.block.*;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.*;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.ShulkerEntity;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.nbt.*;
import net.minecraft.util.Pair;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.world.GameRules;
import net.minecraft.world.World;
import net.sssubtlety.anvil_crushing_recipes.*;
import net.sssubtlety.anvil_crushing_recipes.mixin_helpers.FallingBlockEntityMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;


@Mixin(FallingBlockEntity.class)
public abstract class FallingBlockEntityMixin extends Entity implements FallingBlockEntityMixinAccessor {
    @Shadow public abstract BlockState getBlockState();

    @Shadow public abstract boolean collides();

//    @Shadow public CompoundTag blockEntityData;

    private Pair<Match, AnvilCrushingRecipe> recipeMatch;

    private List<AorB<BlockState, EntityType<?>>> crushedList;
    
    public FallingBlockEntityMixin(EntityType<?> type, World world) {
        super(type, world);
        throw new IllegalStateException("FallingBlockEntityMixin's dummy constructor called. ");
    }

    @Inject(method = "<init>(Lnet/minecraft/entity/EntityType;Lnet/minecraft/world/World;)V", at = @At("TAIL"))
    private void postConstruction(CallbackInfo ci) {
        // can't do instanceof AnvilBlock check here,
        // it's not always true for AnvilBlocks
        // initialize optionalCrushingRecipe so null checks aren't necessary
        recipeMatch = AnvilCrushingRecipe.NO_MATCH;
        crushedList = new LinkedList<>();
    }

    @Redirect(method = "tick", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/FallingBlockEntity;remove()V"),
            slice = @Slice(from = @At(value = "INVOKE", target ="Lnet/minecraft/world/World;removeBlock(Lnet/minecraft/util/math/BlockPos;Z)Z"),
//                    to = @At(value = "FIELD", target = "Lnet/minecraft/block/Blocks;MOVING_PISTON:Lnet/minecraft/block/Block;")))
                            to = @At(value = "INVOKE", target = "Lnet/minecraft/block/BlockState;canReplace(Lnet/minecraft/item/ItemPlacementContext;)Z")))
    private void removeNonAnvils(FallingBlockEntity fallingBlockEntity) {
        Supplier<Boolean> vanillaBehavior = () -> {
            this.remove();
            return true;
        };

        if (notAnvil()) vanillaBehavior.get();

        else if (this.asFallingBlock().timeFalling == 1)// && !justBrokeBlock())
            vanillaBehavior.get();
    }

    @Redirect(method = "tick", at = @At(value = "INVOKE", target =
            "Lnet/minecraft/block/BlockState;canReplace(Lnet/minecraft/item/ItemPlacementContext;)Z"))
    private boolean canReplaceHijack(BlockState blockState, ItemPlacementContext context) {
//        if (this.world.isClient) return false;

        Supplier<Boolean> vanillaBehavior = () -> blockState.canReplace(context);
        // if not anvil, do vanilla thing
        if (notAnvil()) return vanillaBehavior.get();

        if(blockState.isAir()) {
            //shulker check
            BlockPos pos = context.getBlockPos();
            List<Entity> entitiesAtPos = world.getOtherEntities(this, new Box(pos.down()), entity -> entity instanceof ShulkerEntity);
            for (Entity entity : entitiesAtPos)
                // TODO: force canReplace = true if there's recipe
                if (handleEntityRecipe(entity, pos)) break;

            // can replace air and recipes not allowed for air
            return true;
        }

        BlockPos pos = this.getBlockPos();
        BlockState state = world.getBlockState(pos);
        crushedList.add(AorB.ofA(state));

        recipeMatch = AnvilCrushingRecipe.getRecipeMatch(crushedList);

        Match match = AnvilCrushingRecipe.handleBlockMatch(state, recipeMatch, pos, world, this.asFallingBlock());

        boolean canReplace;
        if (match != Match.NONE) {
            if (recipeMatch.getRight().blockOutput == null)
                canReplace = true;//vanillaBehavior.get();
            else {
                crushedList.clear();
                anvilPlacementCodeDupe(blockState, context.getBlockPos().up());
                canReplace = false;
            }
        } else {
            // remove entity if no recipe and can't replace b/c we disabled the vanilla removal,
            // AnvilBlockMixin.onLanding (which is where it would be removed if there was a recipe)
            // won't be reached b/c can't replace
            canReplace = vanillaBehavior.get();
            if (!canReplace)
                this.remove();

        }

        if (canReplace) postRecipe();

        return canReplace;

    }

    @Redirect(method = "tick", at = @At(value = "INVOKE", target =
            "Lnet/minecraft/entity/FallingBlockEntity;dropItem(Lnet/minecraft/item/ItemConvertible;)Lnet/minecraft/entity/ItemEntity;"))
    private ItemEntity dropItemIfNoRecipe(FallingBlockEntity fallingBlockEntity, ItemConvertible item) {
//        if (this.world.isClient) return null;


        Supplier<ItemEntity> vanillaBehavior = () -> this.dropItem(getBlockState().getBlock());
        // if not anvil, do vanilla thing
        if (notAnvil()) return vanillaBehavior.get();

        boolean noRecipe = recipeMatch.getLeft() == Match.NONE;
        postRecipe();
        if (noRecipe && !justBrokeBlock()) {
            // if dropItemIfNoRecipe is reached, it means the anvil tried and failed to place
            // if there was no recipe, we need to drop the item as in vanilla
            // isAir check is in case we just broke a block, we shouldn't drop the anvil as an item
            // also remove, just to be safe
            this.remove();
            return vanillaBehavior.get();
        }
        return null;
    }

    @Redirect(method = "tick", at = @At(value = "INVOKE", target =
            "Lnet/minecraft/world/World;setBlockState(Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;I)Z"))
    private boolean setBlockStateIfNotDone(World world, BlockPos pos, BlockState state, int flags) {
//        if (this.world.isClient) return false;

        Supplier<Boolean> vanillaBehavior = () -> world.setBlockState(pos, state, 3);
        // if not anvil, do vanilla thing
        if (notAnvil()) return vanillaBehavior.get();

        if (recipeMatch.getLeft() == Match.FULL &&
                recipeMatch.getRight().blockOutput != null) {
            // if shouldLand and there was recipe,
            // canReplaceFake already placed block,
            // so prevent normal block placement
            postRecipe();
            return true;
        }

        return vanillaBehavior.get();
    }

    @Redirect(method = "handleFallDamage", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/Entity;damage(Lnet/minecraft/entity/damage/DamageSource;F)Z"))
    private boolean damageOrGetRecipe(Entity entity, DamageSource damageSource, float fallHurtAmount) {
        Supplier<Boolean> vanillaBehavior = () -> entity.damage(damageSource, fallHurtAmount);
        if (notAnvil()) return vanillaBehavior.get();
        return handleEntityRecipe(entity, entity.getBlockPos()) || vanillaBehavior.get();
    }

    @Inject(method = "writeCustomDataToTag", at = @At("TAIL"))
    protected void postWriteCustomDataToTag(CompoundTag tag, CallbackInfo ci) {
        if (notAnvil()) return;
        AnvilCrushingUtil.writeCrushedListToTag(tag, crushedList);
    }

    @Inject(method = "readCustomDataFromTag", at = @At("TAIL"))
    protected void postReadCustomDataFromTag(CompoundTag tag, CallbackInfo ci) {
        if (notAnvil()) return;
        AnvilCrushingUtil.readCrushedListFromTag(tag, crushedList);
    }

    private void anvilPlacementCodeDupe(BlockState blockStateAtPos, BlockPos pos) {
        BlockState thisBlockState = getBlockState();
        Block thisBlock = thisBlockState.getBlock();

        BlockState stateAtPos = world.getBlockState(pos);
        if (!stateAtPos.isAir()) {
            crushedList.add(AorB.ofA(stateAtPos));
            recipeMatch = AnvilCrushingRecipe.getRecipeMatch(crushedList);
            AnvilCrushingRecipe.handleBlockMatch(stateAtPos, recipeMatch, pos, world, this.asFallingBlock());
            crushedList.clear();
        }

        if (this.world.setBlockState(pos, thisBlockState, 3)) {
            if (thisBlock instanceof FallingBlock) {
                ((FallingBlock)thisBlock).onLanding(this.world, pos, thisBlockState, blockStateAtPos, (FallingBlockEntity) (Object)this);
            }

            if (this.asFallingBlock().blockEntityData != null && thisBlock instanceof BlockEntityProvider) {
                BlockEntity blockEntity = this.world.getBlockEntity(pos);
                if (blockEntity != null) {
                    CompoundTag compoundTag = blockEntity.toTag(new CompoundTag());

                    for (String string : this.asFallingBlock().blockEntityData.getKeys()) {
                        Tag tag = this.asFallingBlock().blockEntityData.get(string);
                        if (tag != null && !"x".equals(string) && !"y".equals(string) && !"z".equals(string)) {
                            compoundTag.put(string, tag.copy());
                        }
                    }

                    blockEntity.fromTag(thisBlockState, compoundTag);
                    blockEntity.markDirty();
                }
            }
        } else if (this.asFallingBlock().dropItem && this.world.getGameRules().getBoolean(GameRules.DO_ENTITY_DROPS))
            this.dropItem(thisBlock);

    }

    private boolean notAnvil() {
        return !(this.getBlockState().getBlock() instanceof AnvilBlock);
    }

    FallingBlockEntity asFallingBlock() {
        return ((FallingBlockEntity)(Object)this);
    }

    private boolean handleEntityRecipe(Entity entity, BlockPos pos) {
        crushedList.add(AorB.ofB(entity.getType()));
//        recipeMatch = AnvilCrushingRecipe.getRecipeMatch(crushedList);
//        if (recipeMatch.isPresent()) {
//            if (!recipeMatch.get().crushEntity(entity.getBlockPos(), world, entity))
//                //entity could not be killed, mark empty
//                recipeMatch = Optional.empty();
//            return true;
//        }
        recipeMatch = AnvilCrushingRecipe.getRecipeMatch(crushedList);

        Match match = AnvilCrushingRecipe.handleEntityMatch(entity, recipeMatch, pos, world, this.asFallingBlock());
        postRecipe();
        return match != Match.NONE;
    }

    private void postRecipe() {
        Match matchType = recipeMatch.getLeft();
        if (!this.isSilent() && (matchType == Match.PARTIAL || (matchType == Match.FULL && recipeMatch.getRight().blockOutput == null)))
            world.syncWorldEvent(1031, this.getBlockPos(), 0);

        if (matchType != Match.PARTIAL) {
            crushedList.clear();
            recipeMatch = AnvilCrushingRecipe.NO_MATCH;
        }
    }

    private boolean justBrokeBlock() {
        return world.getBlockState(asFallingBlock().getBlockPos()).isAir();
    }

    @Override
    public Pair<Match, AnvilCrushingRecipe> getRecipeMatch() {
        return recipeMatch;
    }

    @Override
    public List<AorB<BlockState, EntityType<?>>> getCrushedList() {
        return crushedList;
    }

    @Override
    public void setCrushedList(List<AorB<BlockState, EntityType<?>>> crushedList) {
        this.crushedList = crushedList;
    }
}
