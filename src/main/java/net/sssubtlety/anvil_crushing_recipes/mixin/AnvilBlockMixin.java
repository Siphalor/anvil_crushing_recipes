package net.sssubtlety.anvil_crushing_recipes.mixin;

import net.minecraft.block.*;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.FallingBlockEntity;
import net.minecraft.util.Pair;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.sssubtlety.anvil_crushing_recipes.*;
import net.sssubtlety.anvil_crushing_recipes.mixin_helpers.FallingBlockEntityMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Mixin(AnvilBlock.class)
public abstract class AnvilBlockMixin extends FallingBlock implements BlockEntityProvider {

	public AnvilBlockMixin(AbstractBlock.Settings settings) {
		super(settings);
		throw new IllegalStateException("AnvilBlockMixin's dummy constructor called. ");
	}

	@Inject(method = "onLanding", at = @At("HEAD"))
	public void preOnLanding(World world, BlockPos pos, BlockState fallingBlockState, BlockState currentStateInPos, FallingBlockEntity fallingBlockEntity, CallbackInfo ci) {

		List<AorB<BlockState, EntityType<?>>> crushedList = getFECrushedList(fallingBlockEntity);
		setBECrushedList(crushedList, world, pos);
		boolean justCrushed = getFERecipeMatch(fallingBlockEntity).getLeft() == Match.FULL;
		fallingBlockEntity.remove();
		if (!justCrushed)
			handleStateAndRecipes(crushedList, world, pos, pos.down(), 1);
	}

	@Inject(method = "onDestroyedOnLanding", at = @At("HEAD"))
	public void preOnDestroyedOnLanding(World world, BlockPos pos, FallingBlockEntity fallingBlockEntity, CallbackInfo ci) {
		fallingBlockEntity.remove();
		handleStateAndRecipes(getFECrushedList(fallingBlockEntity), world, pos, pos, 2);
	}

	@Inject(method = "configureFallingBlockEntity", at = @At("TAIL"))
	void postOnConfigureFallingBlockEntity(FallingBlockEntity fallingBlockEntity, CallbackInfo ci) {
		((FallingBlockEntityMixinAccessor)fallingBlockEntity).setCrushedList(getBECrushedList(fallingBlockEntity.world, fallingBlockEntity.getBlockPos()));
	}

	public BlockEntity createBlockEntity(BlockView world) {
		return new AnvilBlockEntity();
	}

	private void handleStateAndRecipes(List<AorB<BlockState, EntityType<?>>> crushedList, World world, BlockPos anvilPos, BlockPos startPos, int depth) {
		for (int i = 0; i < depth; i++) {
			BlockState state = world.getBlockState(startPos);
			if (!state.isAir()) {
				crushedList.add(AorB.ofA(state));
				Match match = AnvilCrushingRecipe.handleBlockMatch(state, AnvilCrushingRecipe.getRecipeMatch(crushedList), startPos, world, null);

				if (match != Match.PARTIAL)
					crushedList.clear();
			}

			startPos = startPos.down();
		}
		setBECrushedList(crushedList, world, anvilPos);
	}



	private List<AorB<BlockState, EntityType<?>>> getFECrushedList(FallingBlockEntity fallingBlockEntity) {
		return ((FallingBlockEntityMixinAccessor)fallingBlockEntity).getCrushedList();
	}

	private Pair<Match, AnvilCrushingRecipe> getFERecipeMatch(FallingBlockEntity fallingBlockEntity) {
		return ((FallingBlockEntityMixinAccessor)fallingBlockEntity).getRecipeMatch();
	}

	private void setBECrushedList(List<AorB<BlockState, EntityType<?>>> crushedList, World world, BlockPos pos) {
		AnvilBlockEntity anvilBlockEntity = getAnvilBlockEntity(world, pos);
		if (anvilBlockEntity != null)
			anvilBlockEntity.setCrushedList(crushedList);

	}

	private List<AorB<BlockState, EntityType<?>>> getBECrushedList(World world, BlockPos pos) {
		AnvilBlockEntity anvilBlockEntity = getAnvilBlockEntity(world, pos);
		if (anvilBlockEntity != null) {
			List<AorB<BlockState, EntityType<?>>> BECrushedList = anvilBlockEntity.getCrushedList();
			return BECrushedList == null ? new LinkedList<>() : BECrushedList;
		}

		return new ArrayList<>();
	}

	private AnvilBlockEntity getAnvilBlockEntity(World world, BlockPos pos) {
		BlockEntity blockEntity = world.getBlockEntity(pos);
		if (blockEntity instanceof AnvilBlockEntity)
			return (AnvilBlockEntity)blockEntity;
		else
			LOGGER.warn("Found unexpected BlockEntity type: '" + (blockEntity == null ? "null" : blockEntity.getType().toString()) +
					"'; expected AnvilBlockEntity. ");
		return null;
	}
}
