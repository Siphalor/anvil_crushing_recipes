package net.sssubtlety.anvil_crushing_recipes;

import javafx.collections.ModifiableObservableListBase;

import java.util.*;

public class ObservedList<T> extends ModifiableObservableListBase<T> {
    private final List<T> delegate;

    public ObservedList() {
        this.delegate = new LinkedList<>();
    }

    public ObservedList(Collection<T> collection) {
        this.delegate = new ArrayList<>(collection);
    }

    @Override
    public T get(int index) {
        return delegate.get(index);
    }

    @Override
    public int size() {
        return delegate.size();
    }

    @Override
    protected void doAdd(int index, T element) {
        delegate.add(index, element);
    }

    @Override
    protected T doSet(int index, T element) {
        return delegate.set(index, element);
    }

    @Override
    protected T doRemove(int index) {
        return delegate.remove(index);
    }
}
