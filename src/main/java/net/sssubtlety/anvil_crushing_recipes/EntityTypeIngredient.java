package net.sssubtlety.anvil_crushing_recipes;

import com.google.common.collect.ImmutableSet;
import com.google.gson.JsonElement;
import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.Ingredient;
import net.minecraft.tag.ServerTagManagerHolder;
import net.minecraft.util.registry.Registry;
import net.sssubtlety.anvil_crushing_recipes.mixin.SpawnEggItemAccessor;

import java.util.LinkedList;
import java.util.Set;
import java.util.function.Predicate;

import static net.sssubtlety.anvil_crushing_recipes.mixin.SpawnEggItemAccessor.getSPAWN_EGGS;

public class EntityTypeIngredient extends GenericIngredient<EntityType<?>> {
    public static final EntityTypeIngredient EMPTY = new EntityTypeIngredient();
    public final ImmutableSet<EntityType<?>> matchingEntityTypes;
    protected int[] entityRegistryIndicesCache;
    private static final String TYPE_STRING = "entity";
    private Ingredient asIngredientCache;
    private static final int INDEX = register(TYPE_STRING, EntityTypeIngredient::fromJsonImpl, EntityTypeIngredient::readFromPacketImpl);

    private static EntityTypeIngredient fromJsonImpl(JsonElement json) {
        return new EntityTypeIngredient(listFromJson(json, Registry.ENTITY_TYPE, ServerTagManagerHolder.getTagManager().getEntityTypes(), TYPE_STRING));
    }

    private static EntityTypeIngredient readFromPacketImpl(PacketByteBuf buf) {
        int[] indexArray = buf.readIntArray();
        ImmutableSet.Builder<EntityType<?>> setBuilder = new ImmutableSet.Builder<>();
        for (int id : indexArray) {
            setBuilder.add(Registry.ENTITY_TYPE.get(id));
        }
        return new EntityTypeIngredient(setBuilder.build());
    }

//    protected static final BiPredicate<Object, Object> testImpl = (entityTypeIngredient, entityType) -> {
//        if (!(entityTypeIngredient instanceof EntityTypeIngredient))
//            throw new IllegalArgumentException("entityTypeIngredient not instanceof EntityTypeIngredient. ");
//        if (!(entityType instanceof EntityType))
//            throw new IllegalArgumentException("entityType not instanceof EntityType. ");
//
//        return ((EntityTypeIngredient)entityTypeIngredient).matchingEntityTypes.contains(entityType);
//    };

//    static {
//        INDEX
//    }

    private EntityTypeIngredient() {
        this(new LinkedList<>());
    }

    public EntityTypeIngredient(Iterable<EntityType<?>> entities) {
        matchingEntityTypes = ImmutableSet.copyOf(entities);
    }

    public EntityTypeIngredient(ImmutableSet<EntityType<?>> entities) {
        matchingEntityTypes = entities;
    }



    @Override
    public Ingredient asIngredient() {
        if (asIngredientCache == null)
            asIngredientCache =  ingredientFromItemStackCollection(
                    AnvilCrushingUtil.convertCollection(matchingEntityTypes, entityType ->
                    new ItemStack(getSPAWN_EGGS().get(entityType))));

        return asIngredientCache;
    }

    @Override
    public Set<EntityType<?>> getMatchingSet() {
        return matchingEntityTypes;
    }

    @Override
    protected void subWriteToPacket(PacketByteBuf buf) {
        buf.writeIntArray(getEntityRegistryIndices());
    }

//    @Override
//    protected boolean recipeMatchDelegate(Item item) {
//        return item instanceof SpawnEggItem &&
//                this.test(((SpawnEggItemAccessor) item).getType());
//    }

    @Override
    public boolean matches(Object candidate) {
//        if (candidate instanceof EntityType<?>)
//            return this.test((EntityType<?>) candidate);
//        else
//            return false;
        return candidate instanceof SpawnEggItem &&
                this.test(((SpawnEggItemAccessor) candidate).getType());
    }

    @Override
    protected int getIndex() {
        return INDEX;
    }

    public void write(PacketByteBuf buf) {
        buf.writeIntArray(getEntityRegistryIndices());
    }

    @Override
    public boolean test(EntityType<?> entityType) {
        return this.matchingEntityTypes.contains(entityType);
    }

    @Override
    public Predicate<EntityType<?>> and(Predicate<? super EntityType<?>> other) {
        return block -> test(block) && other.test(block);
    }

    @Override
    public Predicate<EntityType<?>> negate() {
        return block -> !test(block);
    }

    protected int[] getEntityRegistryIndices() {
        if (entityRegistryIndicesCache == null)
            entityRegistryIndicesCache = getRawIds(matchingEntityTypes, Registry.ENTITY_TYPE);

        return entityRegistryIndicesCache;
    }


}
