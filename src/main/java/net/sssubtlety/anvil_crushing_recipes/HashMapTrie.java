package net.sssubtlety.anvil_crushing_recipes;

import net.minecraft.util.Pair;

import java.util.*;

public class HashMapTrie<K, V> {
    public static final Pair<Match, ?> PARTIAL_MATCH = new Pair<>(Match.PARTIAL, null);
    public static final Pair<Match, ?> NO_MATCH = new Pair<>(Match.NONE, null);

    protected Node<K, V> root;

    public HashMapTrie() {
        root = Node.map(new LinkedHashMap<>());
    }

    public void put(V value, List<K> keys) {
        Node<K, V> curNode = root;
        for (K key : keys) {
            Node<K, V> nextNode = curNode.get(key);
            if (nextNode == null)
                nextNode = Node.map();

            curNode.put(key, nextNode);
            curNode = nextNode;
        }

//        curNode.clear();
        curNode.setValue(value);
    }

    public Pair<Match, V> get(List<K> keys) {
        Node<K, V> curNode = root;
        for (int i = 0; i < keys.size(); i++) {
            Node<K, V> nextNode = curNode.get(keys.get(i));
            if (nextNode == null)
                return (Pair<Match,V>) (i == 0 ? NO_MATCH : PARTIAL_MATCH);

            curNode = nextNode;
        }

        return new Pair<>(Match.FULL, curNode.getValue());
    }

    protected static class Node<K, V> {
        protected AorB<LinkedHashMap<K, Node<K, V>>, V> container;

        private Node(AorB<LinkedHashMap<K, Node<K, V>>, V> container) {
            this.container = container;
        }

        public static <K, V> Node<K, V> map(LinkedHashMap<K, Node<K, V>> map) {
            return new Node<>(AorB.ofA(map));
        }

        public static <K, V> Node<K, V> map() {
            return new Node<>(AorB.ofA(new LinkedHashMap<>()));
        }

//        public static <K, V> Node<K, V> value(V value) {
//            return new Node<>(AorB.ofB(value));
//        }

        public void setValue(V value) {
            assert container.isA;
            this.container = AorB.ofB(value);
        }

        public Node<K, V> get(K key) {
            assert container.isA;
            return container.a.get(key);
        }

        public V getValue() {
            return container.b;
        }

//        public void clear() {
//            assert container.isA;
//            container.a.clear();
//        }

        public void put(K key, Node<K, V> node) {
            assert container.isA;
            container.a.put(key, node);
        }
    }

}
