package net.sssubtlety.anvil_crushing_recipes;

public enum Match {
    FULL,
    PARTIAL,
    NONE
}
