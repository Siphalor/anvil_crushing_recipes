package net.sssubtlety.anvil_crushing_recipes.mixin_helpers;

import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityType;
import net.minecraft.util.Pair;
import net.sssubtlety.anvil_crushing_recipes.AnvilCrushingRecipe;
import net.sssubtlety.anvil_crushing_recipes.AorB;
import net.sssubtlety.anvil_crushing_recipes.Match;

import java.util.List;

public interface FallingBlockEntityMixinAccessor {
    Pair<Match, AnvilCrushingRecipe> getRecipeMatch();
    List<AorB<BlockState, EntityType<?>>> getCrushedList();
    void setCrushedList(List<AorB<BlockState, EntityType<?>>> crushedList);
}
