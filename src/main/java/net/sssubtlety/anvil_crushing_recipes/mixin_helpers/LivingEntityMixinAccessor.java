package net.sssubtlety.anvil_crushing_recipes.mixin_helpers;

import net.minecraft.entity.damage.DamageSource;
import org.spongepowered.asm.mixin.Mixin;

public interface LivingEntityMixinAccessor {
    void customKill(DamageSource source, Boolean dropLoot);
}
