package net.sssubtlety.anvil_crushing_recipes;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.Ingredient;
import net.minecraft.tag.Tag;
import net.minecraft.tag.TagGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.registry.Registry;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public abstract class GenericIngredient <T> implements Predicate<T> {
    private static final List<Function<PacketByteBuf, GenericIngredient<?>>>
            bufReaders = new ArrayList<>();
//
//    private static final HashMap<Class<?>, BiPredicate<Object, Object>> testers = new HashMap<>();
//
    private static final HashMap<String, Function<JsonElement, GenericIngredient<?>>>
            jsonReaders = new HashMap<>();

//    private static final HashMap<Class<?>, Function<Class<?>, ? extends GenericIngredient<?>>> casters = new HashMap<>();

    private static final String INGREDIENT_SUFFIX = "_ingredient";

    public final void init() { }

    protected abstract int getIndex();

//    public static List<String> getIngredientTypes() {
//        return new ArrayList<>(jsonReaders.keySet());
//    }

    final public void writeToPacket(PacketByteBuf buf) {
        buf.writeInt(getIndex());
        subWriteToPacket(buf);
    }

    protected abstract void subWriteToPacket(PacketByteBuf buf);

    protected static int register(String typeString, Function<JsonElement, GenericIngredient<?>> jsonReader, Function<PacketByteBuf, GenericIngredient<?>> bufReader) {
        String ingredientTypeString = typeString + INGREDIENT_SUFFIX;
        if (jsonReaders.get(ingredientTypeString) != null) throw new IllegalStateException("Attempting to register jsonReader for " + typeString + " which already has a reader. ");
        jsonReaders.put(ingredientTypeString, jsonReader);
        bufReaders.add(bufReader);

        return bufReaders.size() - 1;
    }

    public abstract boolean matches(Object candidate);

//    protected abstract boolean recipeMatchDelegate(Item item);

    public static GenericIngredient<?> readFromPacket(PacketByteBuf buf) {
        return bufReaders.get(buf.readInt()).apply(buf);
    }

    public static GenericIngredient<?> fromJson(Map.Entry<String ,JsonElement> entry) {
        String key = entry.getKey();
        Function<JsonElement, GenericIngredient<?>> func = jsonReaders.get(key);
        return func.apply(entry.getValue());
    }

    public static GenericIngredient<?> findInJson(JsonObject jsonObject) {
        if (jsonObject.size() < jsonReaders.size()) {
            // check jsonObject elements to see if one has a key mapped in jsonReaders
            Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.entrySet();
            Function<JsonElement, GenericIngredient<?>> reader;
            for (Map.Entry<String, JsonElement> entry : entrySet) {
                reader = jsonReaders.get(entry.getKey());
                if (reader != null)
                    return reader.apply(entry.getValue());
            }
        } else {
            // check jsonReader keys to see if one is in jsonObject
            Set<Map.Entry<String, Function<JsonElement, GenericIngredient<?>>>> entrySet = jsonReaders.entrySet();
            for (Map.Entry<String, Function<JsonElement, GenericIngredient<?>>> entry : entrySet) {
                if (jsonObject.has(entry.getKey()))
                    return entry.getValue().apply(jsonObject.get(entry.getKey()));
            }
        }

        return null;
    }

    public abstract Ingredient asIngredient();

    public abstract Set<T> getMatchingSet();

    protected static <T> int[]  getRawIds(Collection<T> collection, Registry<T> registry) {
        int arraySize = collection.size();
        int[] rawIds = new int[arraySize];
        Iterator<T> tIterator = collection.iterator();
        for (int i = 0; i < arraySize; i++)
            rawIds[i] =registry.getRawId(tIterator.next());
        return rawIds;
    }

    protected static <T> ArrayList<T> listFromJson(JsonElement json, Registry<T> registry, TagGroup<T> tagGroup, String key) {
        if (json == null || json.isJsonNull()) throw new JsonSyntaxException(key + INGREDIENT_SUFFIX + " cannot be empty. ");
        if (json.isJsonObject()) {
            if (JsonHelper.hasElement((JsonObject) json, key)) {
                //T, not tag
                String value = JsonHelper.getString((JsonObject) json, key);
                Optional<T> optionalT = registry.getOrEmpty(new Identifier(value));
                if (optionalT.isPresent())
                    return AnvilCrushingUtil.ArrayListOf(optionalT.get());
                else
                    throw new JsonSyntaxException("Could not find " + key + " matching identifier: '" + value + "'. ");
            } else {
                //tag
                String idString = JsonHelper.getString((JsonObject) json, "tag", "");


                Tag<T> tTag = tagGroup.getTag(new Identifier(idString));

                if (tTag == null)
                    throw new JsonSyntaxException("Can't find " + key + " tag with identifier '" + idString + "'. ");
                if (tTag.values().size() < 1)
                    throw new JsonSyntaxException(AnvilCrushingUtil.capitalize(key) + " tag with identifier '" + idString + "' is empty. ");

                return new ArrayList<>(tTag.values());
            }
        } else if (json.isJsonArray()) {
            JsonArray jsonArray = json.getAsJsonArray();
            if (jsonArray.size() == 0) {
                throw new JsonSyntaxException(key + INGREDIENT_SUFFIX + " array cannot be empty. ");
            } else {
                ArrayList<T> list = new ArrayList<>();
                for (JsonElement element : jsonArray) {
                    if (element.isJsonObject()) {
                        list.addAll(listFromJson(element, registry, tagGroup, key));
                    } else
                        throw new JsonSyntaxException(key + INGREDIENT_SUFFIX + " array may only contain " + key + " or tag objects. ");
                }
                return list;
            }
        } else {
            throw new JsonSyntaxException("Expected element to be object or array of objects. ");
        }

    }

    public static Ingredient ingredientFromItemStackCollection(Collection<ItemStack> itemStacks) {
        return Ingredient.ofStacks(itemStacks.stream());
    }
}
