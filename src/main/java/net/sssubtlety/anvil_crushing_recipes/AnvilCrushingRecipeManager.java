package net.sssubtlety.anvil_crushing_recipes;

import com.google.gson.*;
import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.ShapedRecipe;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.profiler.Profiler;
import net.minecraft.util.registry.Registry;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static net.sssubtlety.anvil_crushing_recipes.AnvilCrushingRecipe.updateAnvilCrushingRecipes;
import static net.sssubtlety.anvil_crushing_recipes.AnvilCrushingRecipesInit.MOD_IDENTIFIER;

public class AnvilCrushingRecipeManager extends JsonDataLoader implements IdentifiableResourceReloadListener {
    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();


    public AnvilCrushingRecipeManager(String dataType) {
        super(GSON, dataType);
    }

    public AnvilCrushingRecipe read(Identifier identifier, JsonObject jsonObject) {
        ArrayList<GenericIngredient<?>> ingredients = new ArrayList<>();
        if (jsonObject.has("ingredients")) {
            JsonArray jsonArray = jsonObject.get("ingredients").getAsJsonArray();

            if (jsonArray.size() == 0) throw new JsonSyntaxException("'ingredients' present but empty. ");
            JsonObject asObject;
            for (JsonElement element : jsonArray) {
                asObject = element.getAsJsonObject();
                if (asObject.size() != 1) throw new JsonSyntaxException("'ingredients' must only contain " +
                        "objects with exactly one element. ");
                // get first (only) asObject entry and create GenericIngredient from it
                Map.Entry<String, JsonElement> first = asObject.entrySet().iterator().next();
                ingredients.add(GenericIngredient.fromJson(first));
            }
        } else {
            GenericIngredient<?> ingredient = GenericIngredient.findInJson(jsonObject);
            if (ingredient == null) throw new JsonSyntaxException("An anvil_crushing_recipe must have at least one ingredient. ");
            ingredients.add(ingredient);
        }

        boolean dropChanceSpecified = jsonObject.has("ingredient_drop_chance");
        float ingredientDropChance = dropChanceSpecified ?
                JsonHelper.getFloat(jsonObject, "ingredient_drop_chance") : 0;

        BlockItem blockOutput = AnvilCrushingRecipeManager.readBlockOutput(jsonObject);
        ArrayList<ItemStack> itemOutputs = readItemOutputs(jsonObject);

        String lootIDString = JsonHelper.getString(jsonObject, "loot_output", "");
        Identifier lootOutputID = lootIDString.equals("") ? null : new Identifier(lootIDString);

        if (ingredients.size() == 1) {
            if (!dropChanceSpecified && blockOutput == null && itemOutputs == null && lootOutputID == null)
                // if only one ingredient and no outputs of any kind specified,
                // assume block_breaker
                ingredientDropChance = 1;
        } else if (dropChanceSpecified) {
            AnvilCrushingRecipesInit.LOGGER.warn("Multiple ingredients specified. Ignoring 'ingredient_drop_chance'. ");
            ingredientDropChance = 0;
        }

        return new AnvilCrushingRecipe(identifier, ingredients, ingredientDropChance, blockOutput, itemOutputs, lootOutputID);
    }

    private static ArrayList<ItemStack> readItemOutputs(JsonObject jsonObject) {
        JsonArray jsonArray = JsonHelper.getArray(jsonObject, "item_outputs", null);
        if (jsonArray == null) return null;

        ArrayList<ItemStack> itemOutputs = new ArrayList<>();

        for(int i = 0; i < jsonArray.size(); ++i) {
            ItemStack itemStack = ShapedRecipe.getItemStack(jsonArray.get(i).getAsJsonObject());
            if (!itemStack.isEmpty()) {
                itemOutputs.add(itemStack);
            }
        }

        return itemOutputs.size() == 0 ? null : itemOutputs;
    }

    public static BlockItem readBlockOutput(JsonObject jsonObject) {
        String idString = JsonHelper.getString(jsonObject, "block_output", "");
        Item item = Registry.ITEM.get(new Identifier(idString));
        if (item instanceof BlockItem)
            return (BlockItem) item;
        else
            return null;
    }

    @Override
    public Identifier getFabricId() {
        return MOD_IDENTIFIER;
    }

    @Override
    protected void apply(Map<Identifier, JsonElement> loader, ResourceManager manager, Profiler profiler) {
        final List<AnvilCrushingRecipe> anvilCrushingRecipes = new LinkedList<>();

        loader.forEach(((identifier, element) -> {
            if (element.isJsonObject()) {
                AnvilCrushingRecipe recipe;
                try {
                    recipe = read(identifier, element.getAsJsonObject());
                    anvilCrushingRecipes.add(recipe);
                } catch (Exception e) {
                    AnvilCrushingRecipesInit.LOGGER.error(e.getMessage());
                }
            }
        }));

        updateAnvilCrushingRecipes(anvilCrushingRecipes);
    }
}
