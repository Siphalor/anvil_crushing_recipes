package net.sssubtlety.anvil_crushing_recipes;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.recipe.RecipeSerializer;
import net.minecraft.recipe.RecipeType;
import net.minecraft.resource.ResourceType;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// TODO: REI integration

public class AnvilCrushingRecipesInit implements ModInitializer {
	public static final Logger LOGGER = LogManager.getLogger();
	public static final String MOD_ID = "anvil_crushing_recipes";
	public static final Identifier MOD_IDENTIFIER = new Identifier(MOD_ID);

//	public static final RecipeSerializer<AnvilCrushingRecipe> ANVIL_CRUSHING_SERIALIZER =
//			Registry.register(Registry.RECIPE_SERIALIZER, new Identifier(MOD_ID, "anvil_crushing"),
//				AnvilCrushingRecipe.SERIALIZER);

//	public static final RecipeType<AnvilCrushingRecipe> ANVIL_CRUSHING_RECIPE_TYPE = Registry.register(Registry.RECIPE_TYPE,
//			new Identifier(MOD_ID, "anvil_crushing"), new RecipeType<AnvilCrushingRecipe>() {
//		public String toString() {
//			return MOD_ID + ":anvil_crushing";
//		}
//	});

	public static final BlockEntityType<AnvilBlockEntity> ANVIL_BLOCK_ENTITY_TYPE =
			Registry.register(Registry.BLOCK_ENTITY_TYPE, "anvil_crushing_recipes:anvil_block_entity", BlockEntityType.Builder.
				create(AnvilBlockEntity::new, Blocks.ANVIL, Blocks.CHIPPED_ANVIL, Blocks.DAMAGED_ANVIL).build(null));

//	private static final ServerLifecycleEvents.ServerStarted startedListener = AnvilCrushingRecipesInit::listenerImpl;
//	private static final ServerLifecycleEvents.EndDataPackReload reloadedListener = (server, resourceManager, bool) -> listenerImpl(server);

//	private static void listenerImpl(MinecraftServer server) {
//		AnvilCrushingRecipe.updateAnvilCrushingRecipes(server.getRecipeManager().listAllOfType(ANVIL_CRUSHING_RECIPE_TYPE));
//	}

	@Override
	public void onInitialize() {
		BlockIngredient.EMPTY.init();
		EntityTypeIngredient.EMPTY.init();
//		ServerLifecycleEvents.SERVER_STARTED.register(startedListener);
//		ServerLifecycleEvents.END_DATA_PACK_RELOAD.register(reloadedListener);
		ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(AnvilCrushingRecipe.MANAGER);
	}
}
