package net.sssubtlety.anvil_crushing_recipes;

import net.minecraft.block.BlockState;
import net.minecraft.entity.EntityType;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import java.util.*;
import java.util.function.Function;

public interface AnvilCrushingUtil {
    String IS_BLOCK_BYTES_KEY = "is_block_bytes";
    String ID_STRINGS_KEY = "id_strings";

    @SafeVarargs
    static <T> ArrayList<T> ArrayListOf(T... elements) {
        ArrayList<T> list = new ArrayList<>();
        Collections.addAll(list, elements);
        return list;
    }

//    static <T extends ItemConvertible> LinkedList<ItemStack> ItemStackListFromItemConvertible(Collection<T> collection) {
//        LinkedList<ItemStack> stackCollection = new LinkedList<>();
//        for (ItemConvertible itemConvertible : collection) {
//            stackCollection.add(new ItemStack(itemConvertible));
//        }
//        return stackCollection;
//    }

//    static int[] getRawBlockIds(Collection<Block> blocks) {
//        int arraySize = blocks.size();
//        int[] blockIDs = new int[arraySize];
//        Iterator<Block> blockItr = blocks.iterator();
//        for (int i = 0; i < arraySize; i++)
//            blockIDs[i] = Registry.BLOCK.getRawId(blockItr.next());
//        return blockIDs;
//    }
//
//    static int[] getRawEntityTypeIds(Collection<EntityType<?>> entityTypes) {
//        int arraySize = entityTypes.size();
//        int[] blockIDs = new int[arraySize];
//        Iterator<EntityType<?>> blockItr = entityTypes.iterator();
//        for (int i = 0; i < arraySize; i++)
//            blockIDs[i] = Registry.ENTITY_TYPE.getRawId(blockItr.next());
//        return blockIDs;
//    }




//    static List<ItemStack> getDroppedStacks(BlockState state, ServerWorld world) {
//        return state.getDroppedStacks(new LootContext.Builder(world));
//    }

//    static void dropStacksAtPos(List<ItemStack> stacks, BlockPos pos, World world) {
//        for (ItemStack stack : stacks)
//            Block.dropStack(world, pos, stack);
//    }
    static String capitalize(String str) {
        if(str == null || str.isEmpty())
            return str;

        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    static <T1, T2> Collection<T2> convertCollection(Collection<T1> source, Function<T1, T2> converter) {
        Collection<T2> dest = new ArrayList<>();
        for (T1 entry : source) {
            dest.add(converter.apply(entry));
        }
        return dest;
    }

    static List<Item> inventoryToItemList(Inventory inv) {
        ArrayList<Item> list = new ArrayList<>();
        final int invSize = inv.size();
        for (int i = 0; i < invSize; i++) {
            ItemStack stack = inv.getStack(i);
            if (!stack.isEmpty())
                list.add(stack.getItem());
        }

        return list;
    }

    static void writeCrushedListToTag(CompoundTag tag, List<AorB<BlockState, EntityType<?>>> crushedList) {
        int numCrushed = crushedList.size();
        byte[] isBlockBytes = new byte[numCrushed];
        ListTag idStrings = new ListTag();

        AorB<BlockState, EntityType<?>> blockStateOrEntityType;
        for (int i = 0; i < numCrushed; i++) {
            blockStateOrEntityType = crushedList.get(i);
            if (blockStateOrEntityType.isA) {
                isBlockBytes[i] = 1;
                idStrings.add(StringTag.of(Registry.BLOCK.getId(blockStateOrEntityType.a.getBlock()).toString()));
            } else {
                isBlockBytes[i] = 0;
                idStrings.add(StringTag.of(Registry.ENTITY_TYPE.getId(blockStateOrEntityType.b).toString()));
            }
        }

        tag.putByteArray(IS_BLOCK_BYTES_KEY, isBlockBytes);
        tag.put(ID_STRINGS_KEY, idStrings);
    }

    static void readCrushedListFromTag(CompoundTag tag, List<AorB<BlockState, EntityType<?>>> crushedList) {
        byte[] isBlockBytes = tag.getByteArray(IS_BLOCK_BYTES_KEY);
        ListTag idStrings = (ListTag) tag.get(ID_STRINGS_KEY);

        if (crushedList == null)
            crushedList = new LinkedList<>();
        else
            crushedList.clear();

        if (idStrings == null) return;

        int numCrushed = idStrings.size();
        Identifier id;
        for (int i = 0; i < numCrushed; i++) {
            id = new Identifier(idStrings.get(i).asString());
            if (isBlockBytes[i] > 0) // is block
                crushedList.add(AorB.ofA(Registry.BLOCK.get(id).getDefaultState()));
            else // is entity
                crushedList.add(AorB.ofB(Registry.ENTITY_TYPE.get(id)));
        }
    }

    static int intAverage(int... integers) {
        int sum = 0;
        for (int integer : integers)
            sum += integer;

        return sum/integers.length;
    }
}
