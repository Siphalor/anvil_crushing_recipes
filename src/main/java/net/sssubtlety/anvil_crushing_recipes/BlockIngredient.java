package net.sssubtlety.anvil_crushing_recipes;

import com.google.gson.JsonElement;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.Ingredient;
import net.minecraft.tag.ServerTagManagerHolder;
import net.minecraft.util.registry.Registry;

import java.util.*;
import java.util.function.Predicate;

public class BlockIngredient extends GenericIngredient<Block> {
    public static final BlockIngredient EMPTY = new BlockIngredient();
    protected final HashSet<Block> matchingBlocks;
    protected Ingredient asIngredientCache = null;
    protected ArrayList<ItemStack> matchingStacksCache = null;
    protected int[] blockRegistryIndicesCache;
    private static final String TYPE_STRING = "block";
    private static final int INDEX = register(TYPE_STRING, BlockIngredient::fromJsonImpl, BlockIngredient::readFromPacketImpl);

    private static BlockIngredient fromJsonImpl(JsonElement json) {
        return new BlockIngredient(listFromJson(json, Registry.BLOCK, ServerTagManagerHolder.getTagManager().getBlocks(), TYPE_STRING));
    }

    private static BlockIngredient readFromPacketImpl(PacketByteBuf buf) {
        int[] indexArray = buf.readIntArray();
        HashSet<Block> blockSet = new HashSet<>();
        for (int id : indexArray) {
            blockSet.add(Registry.BLOCK.get(id));
        }
        return new BlockIngredient(blockSet);
    }

    private BlockIngredient() {
        this(new LinkedList<>());
    }

    public BlockIngredient(Collection<Block> blocks) {
        matchingBlocks = new HashSet<>(blocks);
    }

    public BlockIngredient(HashSet<Block> blocks) {
        matchingBlocks = blocks;
    }

    @Override
    protected void subWriteToPacket(PacketByteBuf buf) {
        buf.writeIntArray(getBlockRegistryIndices());
    }

//    @Override
//    protected boolean recipeMatchDelegate(Item item) {
//        return item instanceof BlockItem &&
//                matchingBlocks.contains(((BlockItem) item).getBlock());
//    }

    @Override
    public boolean matches(Object candidate) {
//        if (candidate instanceof Block)
//            return this.test((Block) candidate);
//        else
//            return false;
        return //candidate instanceof Block &&
                matchingBlocks.contains(candidate);
    }

    @Override
    protected int getIndex() {
        return INDEX;
    }

//    public void write(PacketByteBuf buf) {
//        buf.writeIntArray(getBlockRegistryIndices());
//    }

//    public BlockIngredient(Block... blocks) {
//        this();
//        Collections.addAll(matchingBlocks, blocks);
//    }
//
//    protected void merge(BlockIngredient other) {
//        matchingBlocks.addAll(other.matchingBlocks);
//    }

    @Override
    public Ingredient asIngredient() {
        if(asIngredientCache == null)
            return ingredientFromItemStackCollection(
                    AnvilCrushingUtil.convertCollection(matchingBlocks, ItemStack::new));
        else
            return asIngredientCache;
    }

    @Override
    public Set<Block> getMatchingSet() {
        return matchingBlocks;
    }

    @Override
    public boolean test(Block block) {
        return this.matchingBlocks.contains(block);
    }

    @Override
    public Predicate<Block> and(Predicate<? super Block> other) {
        return block -> test(block) && other.test(block);
    }

    @Override
    public Predicate<Block> negate() {
        return block -> !test(block);
    }

    private ArrayList<ItemStack> getMatchingStacks() {
        if(matchingStacksCache == null) {
            matchingStacksCache = new ArrayList<>();
            for (Block block : matchingBlocks) {
                matchingStacksCache.add(new ItemStack(block));
            }
        }

        return matchingStacksCache;
    }

    protected int[] getBlockRegistryIndices() {
        if (blockRegistryIndicesCache == null)
            blockRegistryIndicesCache = getRawIds(matchingBlocks, Registry.BLOCK);

        return blockRegistryIndicesCache;
    }
}
