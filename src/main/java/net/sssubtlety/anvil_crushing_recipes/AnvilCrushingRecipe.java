package net.sssubtlety.anvil_crushing_recipes;

import net.minecraft.block.BedBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.enums.BedPart;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.FallingBlockEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.item.AutomaticItemPlacementContext;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextType;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.state.property.Property;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import net.sssubtlety.anvil_crushing_recipes.mixin_helpers.LivingEntityMixinAccessor;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;

import static net.sssubtlety.anvil_crushing_recipes.AnvilCrushingUtil.intAverage;

public class AnvilCrushingRecipe {
//    public static final RecipeSerializer<AnvilCrushingRecipe> SERIALIZER = new AnvilCrushingRecipeSerializer();

    public static final AnvilCrushingRecipeManager MANAGER = new AnvilCrushingRecipeManager("anvil_crushing_recipes");

    public static final Pair<Match, AnvilCrushingRecipe> NO_MATCH = new Pair<>(Match.NONE, null);

    public static final Comparator<Object> OBJECT_HASH_COMPARATOR = Comparator.comparingInt(Object::hashCode);

    public static final Comparator<AnvilCrushingRecipe> ANVIL_CRUSHING_RECIPE_COMPARATOR = (recipe1, recipe2) -> {
        List<GenericIngredient<?>> ingredients1 = recipe1.ingredients;
        List<GenericIngredient<?>> ingredients2 = recipe2.ingredients;

        if (recipe1.id == recipe2.id) return 0;

        int ingredients1Size = ingredients1.size();
        int ingredients2Size = ingredients2.size();
        if (ingredients1Size == ingredients2Size) {
            for (int i = 0; i < ingredients1Size; i++) {
                GenericIngredient<?> ingredient1 = ingredients1.get(0);
                GenericIngredient<?> ingredient2 = ingredients2.get(0);
                if (ingredient1 != ingredient2)
                    return ingredient1.hashCode() - ingredients2.hashCode();
            }
            AnvilCrushingRecipesInit.LOGGER.error("ANVIL_CRUSHING_RECIPE_COMPARATOR found recipes with different identifiers but matching ingredients. IDs: " + recipe1.id + " and " + recipe2.id);
            return 0;
        }
        else
            return ingredients1Size - ingredients2Size;
    };

    private static final TreeMap<Block, SortedCollection<AnvilCrushingRecipe>> BLOCK_FIRST_RECIPES = new TreeMap<>(OBJECT_HASH_COMPARATOR);
    private static final TreeMap<EntityType<?>, SortedCollection<AnvilCrushingRecipe>> ENTITY_FIRST_RECIPES = new TreeMap<>(OBJECT_HASH_COMPARATOR);

    public final Identifier id;
    public final List<GenericIngredient<?>> ingredients;
    public final float ingredientDropChance;
    public final BlockItem blockOutput;
    public final ArrayList<ItemStack> itemOutputs;
    public final Identifier lootOutputTableID;

    private LootTable lootOutputTableCache;

    // TODO:
    //  - add 'un-crafting' feature, by recipe type or by recipe name
    //  - add REI integration
    //  - add more parameters:
    //    - anvil speed
    //    - anvil fall distance
    //    - anvil damaged state

    public AnvilCrushingRecipe(Identifier id, List<GenericIngredient<?>> ingredients, float ingredientDropChance, BlockItem blockOutput, ArrayList<ItemStack> itemOutputs, Identifier lootOutputTableID) {
        this.id = id;
        this.ingredients = ingredients;
        this.ingredientDropChance = ingredientDropChance;
        this.blockOutput = blockOutput;
        this.itemOutputs = itemOutputs;
        this.lootOutputTableID = lootOutputTableID;
        this.lootOutputTableCache = null;
    }

    public static void updateAnvilCrushingRecipes(List<AnvilCrushingRecipe> anvilCrushingRecipes) {
//        AnvilCrushingRecipe.allAnvilCrushingRecipes = anvilCrushingRecipes;
        BLOCK_FIRST_RECIPES.clear();
        ENTITY_FIRST_RECIPES.clear();
        for (AnvilCrushingRecipe recipe : anvilCrushingRecipes)
            mapRecipe(recipe, recipe.ingredients.get(0));
    }

    private static <T> void mapRecipe(AnvilCrushingRecipe recipe, GenericIngredient<T> firstIngredient) {
        TreeMap<T, SortedCollection<AnvilCrushingRecipe>> recipes = firstIngredient instanceof BlockIngredient ?
                (TreeMap<T, SortedCollection<AnvilCrushingRecipe>>) BLOCK_FIRST_RECIPES :
                (TreeMap<T, SortedCollection<AnvilCrushingRecipe>>) ENTITY_FIRST_RECIPES;
        for (T entry : firstIngredient.getMatchingSet()) {
            SortedCollection<AnvilCrushingRecipe> matchingFirstIngredientRecipes = recipes.get(entry);
            if (matchingFirstIngredientRecipes == null) {
                matchingFirstIngredientRecipes = new SortedCollection<>(ANVIL_CRUSHING_RECIPE_COMPARATOR);
                recipes.put(entry, matchingFirstIngredientRecipes);
            }
            matchingFirstIngredientRecipes.add(recipe);
        }
    }

    public static Pair<Match, AnvilCrushingRecipe> getRecipeMatch(List<AorB<BlockState, EntityType<?>>> crushedList) {
        AorB<BlockState, EntityType<?>> crushedFirst = crushedList.get(0);
        SortedCollection<AnvilCrushingRecipe> possibleRecipes;
        if (crushedFirst.isA) {
            Block defaultBlock = getDefaultBlock(crushedFirst.a);
            if (defaultBlock == null)
                return NO_MATCH;
            else
                possibleRecipes = BLOCK_FIRST_RECIPES.get(defaultBlock);
        } else
            possibleRecipes = ENTITY_FIRST_RECIPES.get(crushedFirst.b);

        if (possibleRecipes == null || possibleRecipes.isEmpty()) return NO_MATCH;

        AnvilCrushingRecipe firstRecipe = possibleRecipes.get(0);
        if (firstRecipe.ingredients.size() == 1)
            return new Pair<>(Match.FULL, firstRecipe);


        possibleRecipes = withMinIngredients(possibleRecipes, crushedList.size());

        int numCrushed = crushedList.size();
        int i;
        // loop through recipes with ingredients.size() == numCrushed
        // can only be FULL matches
        for (i = 0; i < possibleRecipes.size(); i++) {
            AnvilCrushingRecipe curRecipe = possibleRecipes.get(i);
            List<GenericIngredient<?>> curIngredients = curRecipe.ingredients;
            int curSize = curIngredients.size();
            if (curSize > numCrushed) break;
            int j;
            for (j = 1; j < curSize; j++) {
                AorB<BlockState, EntityType<?>> curCrushed = crushedList.get(j);
                if (!curIngredients.get(j).matches(curCrushed.isA ? getDefaultBlock(curCrushed.a) : curCrushed.b)) break;
            }

            if (j == numCrushed) {
                return new Pair<>(Match.FULL, curRecipe);
            }
        }
        // loop through recipes with ingredients.size() > numCrushed
        // can only be PARTIAL matches
        for (; i < possibleRecipes.size(); i++) {
            AnvilCrushingRecipe curRecipe = possibleRecipes.get(i);
            int j;
            for (j = 1; j < numCrushed; j++) {
                AorB<BlockState, EntityType<?>> curCrushed = crushedList.get(j);
                if (!curRecipe.ingredients.get(j).matches(curCrushed.isA ? getDefaultBlock(curCrushed.a) : curCrushed.b)) break;
            }

            if (j == numCrushed) {
                return new Pair<>(Match.PARTIAL, curRecipe);
            }
        }


        return NO_MATCH;
    }

    private static Block getDefaultBlock(BlockState state) {
        try {
            return ((BlockItem) state.getBlock().asItem()).getBlock();
        } catch (ClassCastException e) {
            return null;
        }
    }

    private void doOutput(BlockPos pos, World world, BlockState replacingState, FallingBlockEntity fallingAnvil) {
        if (replacingState == null)
            placeBlockOutputOrDropItsStacks(pos, world, fallingAnvil);
        else {
            if (blockOutput != null) {
                if (fallingAnvil != null) fallingAnvil.remove();
                if (!placeBlockMatchState(pos, world, replacingState, fallingAnvil))
                    world.syncWorldEvent(null, 2001, pos, Block.getRawIdFromState(replacingState));
            }
        }
        outputItems(pos, world);

        outputLoot(pos, world);
    }

    public static Match handleBlockMatch(BlockState state, Pair<Match, AnvilCrushingRecipe> recipeMatch, BlockPos pos, World world, FallingBlockEntity fallingBlock) {
        Match match = recipeMatch.getLeft();
        AnvilCrushingRecipe recipe = recipeMatch.getRight();

        if (match != Match.NONE) {
            recipe.crushBlock(pos, world, state);
            if (match == Match.FULL)
                recipe.doOutput(pos, world, state, fallingBlock);
        }
        return match;
    }

    public static Match handleEntityMatch(Entity entity, Pair<Match, AnvilCrushingRecipe> recipeMatch, BlockPos pos, World world, FallingBlockEntity fallingBlock) {
        Match match = recipeMatch.getLeft();
        AnvilCrushingRecipe recipe = recipeMatch.getRight();

        if (match != Match.NONE) {
            recipe.crushEntity(pos, world, entity);
            if (match == Match.FULL)
                recipe.doOutput(pos, world, null, fallingBlock);
        }

        return match;
    }

    private static SortedCollection<AnvilCrushingRecipe> withMinIngredients(SortedCollection<AnvilCrushingRecipe> possibleRecipes, int min) {
        if (possibleRecipes.isEmpty()) return possibleRecipes;

        int first = 0;
        int last = possibleRecipes.size() - 1;
        int mid = intAverage(first, last);
        //binary search
        while(true) {
            int midSize = possibleRecipes.get(mid).ingredients.size();

            if (midSize == min) return possibleRecipes.subList(mid, possibleRecipes.size());
            else if (midSize > min) {
                if (last == mid) break;
                last = mid;
                mid = intAverage(first, last);
            }
            else /*(midSize < min)*/ {
                if (first == mid) break;
                first = mid;
                mid = intAverage(first, last);
            }
        }
        while (mid > 0 && possibleRecipes.get(mid).ingredients.size() == possibleRecipes.get(mid - 1).ingredients.size()) {
            mid--;
        }

        return possibleRecipes.subList(mid, possibleRecipes.size());
    }

    public void crushBlock(BlockPos pos, World world, BlockState state) {
        world.breakBlock(pos, shouldDropIngredient(world));
    }

    public boolean crushEntity(BlockPos pos, World world, Entity entity) {
        // return false if entity is already dead or is invulnerable
        if (!entity.isAlive() || entity.isInvulnerable()) return false;

        if (entity instanceof LivingEntity) {
            ((LivingEntityMixinAccessor) entity).customKill(DamageSource.ANVIL, shouldDropIngredient(world));
        } else
            entity.kill();

        // successfully killed entity, return true
        return true;
    }

    private void outputItems(BlockPos pos, World world) {
        if (itemOutputs != null)
            for (ItemStack stack : itemOutputs)
                Block.dropStack(world, pos, stack.copy());
    }

    private void outputLoot(BlockPos pos, World world) {
        LootTable lootOutput = getLootOutput(world.getServer());
        if (lootOutput != LootTable.EMPTY && world instanceof ServerWorld)
            for (ItemStack stack : lootOutput.generateLoot(new LootContext.Builder((ServerWorld) world).build(new LootContextType.Builder().build())))
                Block.dropStack(world, pos, stack.copy());
    }

    private boolean shouldDropIngredient(World world) {
        return this.ingredientDropChance == 1 || (this.ingredientDropChance > 0 && world.random.nextFloat() < this.ingredientDropChance);
    }

    private boolean placeBlockMatchState(BlockPos pos, World world, BlockState state, FallingBlockEntity fallingAnvil) {
        if (!placeBlockOutputOrDropItsStacks(pos, world, fallingAnvil)) return false;

        BlockState updatedState = null;
        if (state.getBlock().getClass().isInstance(blockOutput.getBlock())) {
            updatedState = world.getBlockState(pos);
            //copy properties from parent to child
            for (Property property : state.getProperties()) {
                Comparable value = state.get(property);
                updatedState = updatedState.with(property, value);
            }
        }

        if (updatedState != null)
            world.setBlockState(pos, updatedState);

        return true;
    }

    /***
     * Tries to place blockOutput if present. If there's no block output, or if it succeeds,
     * it returns true. Otherwise it drops the stacks of the defaultState of the
     * blockOutput and returns false
     */
    private boolean placeBlockOutputOrDropItsStacks(BlockPos pos, World world, FallingBlockEntity fallingAnvil) {
        // returns true if there was blockPlacement failure
        if (this.blockOutput == null) return true;
        if (fallingAnvil != null) fallingAnvil.remove();
        if (!tryPlace(pos, world)) {
            //could not place block
            //drop stacks instead
//            //UNLESS we tried to place inside an anvil,
//            //then we assume we were pushed here by a piston, drop nothing
//            if (!(world.getBlockState(pos).getBlock() instanceof AnvilBlock))
            Block block = blockOutput.getBlock();
            BlockState defaultState = block.getDefaultState();
            // special handling for BedBlock because default state is foot which has no drops
            if (block instanceof BedBlock) defaultState = defaultState.with(BedBlock.PART, BedPart.HEAD);
            Block.dropStacks(defaultState, world, pos);
            return false;
        }
        return true;
    }

    private boolean tryPlace(BlockPos pos, World world) {
        for (Direction direction : Direction.values()) {
            if (ActionResult.CONSUME == this.blockOutput.place(new AutomaticItemPlacementContext(world, pos, direction, new ItemStack(blockOutput), Direction.DOWN)))
                return true;
        }
        return false;
    }

    public LootTable getLootOutput(MinecraftServer server) {
        if (lootOutputTableCache == null) {
            if (lootOutputTableID == null)
                lootOutputTableCache = LootTable.EMPTY;
            else {
                lootOutputTableCache = server.getLootManager().getTable(lootOutputTableID);
                if (lootOutputTableCache == LootTable.EMPTY)
                    AnvilCrushingRecipesInit.LOGGER.warn("No loot table matching identifier: '" + lootOutputTableID.toString() + "'. ");
            }
        }
        return lootOutputTableCache;
    }

//    protected static Item getInvFirstItem(Inventory inv) {
//        return inv.getStack(0).getItem();
//    }

}
