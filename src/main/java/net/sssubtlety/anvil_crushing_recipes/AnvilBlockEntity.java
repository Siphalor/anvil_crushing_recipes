package net.sssubtlety.anvil_crushing_recipes;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.nbt.CompoundTag;

import java.util.LinkedList;
import java.util.List;

public class AnvilBlockEntity extends BlockEntity {
    private List<AorB<BlockState, EntityType<?>>> crushedList;

    public AnvilBlockEntity(List<AorB<BlockState, EntityType<?>>> crushedList) {
        super(AnvilCrushingRecipesInit.ANVIL_BLOCK_ENTITY_TYPE);
        this.crushedList = crushedList;
    }

    public AnvilBlockEntity() {
        this(new LinkedList<>());
    }

    @Override
    public void fromTag(BlockState state, CompoundTag tag) {
        super.fromTag(state, tag);
        AnvilCrushingUtil.readCrushedListFromTag(tag, crushedList);
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        super.toTag(tag);
        AnvilCrushingUtil.writeCrushedListToTag(tag, crushedList);
        return tag;
    }

    public List<AorB<BlockState, EntityType<?>>> getCrushedList() {
        return crushedList;
    }

    public void setCrushedList(List<AorB<BlockState, EntityType<?>>> crushedList) {
        this.crushedList = crushedList;
    }
}
