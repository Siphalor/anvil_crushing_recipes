- 1.0 (16 Jan. 2021): First full release!
  Marked as compatible with 1.16.5. 
- 0.8.5-alpha (6 Jan. 2021): This is an ***alpha*** version. I made some non-trivial internal changes and may have overlooked something, introducing new issues. 
  
  Recipes now 'try harder' to place their block outputs by trying to place in each direction. 
  
  Fixed a bug where anvils would pop into item form after breaking a partial block with air below it. 
  
  Fixed a crash that occurred when falling anvils landed on moving blocks (pushed by pistons).
  
  Fixed an issue where bed block outputs that failed to place wouldn't drop a bed item. There're likely more issues like this related to failing to place multi-blocks, but unfortunately there's no universal fix. 

- 0.8.4-beta (11 Dec. 2020): Switched from using Minecraft's built-in recipe system to using a custom data type. 

  If you've made custom anvil crushing recipe datapacks, simply rename your `recipes` folder to `anvil_crushing_recipes`. 
  
  You may also remove the `"type": "anvil_crushing_recipes:anvil_crushing"` line, but leaving it in won't cause any problems. 
  
  Added a new recipe to 'Default Anvil Crushing Recipes': `cracked_nether_bricks_to_netherrack`.
- 0.8.3-alpha-3 (2 Dec. 2020): Hotfix for 'unexpected BlockEntity type' log spam reported in [issue 10](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/issues/10). 
- 0.8.3-alpha-2 (1 Dec. 2020): Hotfix for crash reported in [#9](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/issues/9). 
- 0.8.3-alpha-1 (11 Nov. 2020): This is an ***alpha*** version. I've made extensive changes internally, so it's more likely than normal that I've introduced new issues. 
  Marked as compatible with 1.16.4
  Added support for multiple inputs!
  Your old, single-ingredient recipes should still work
  Define multiple ingredients in the same way as before, but inside an array: `"ingredients": [...]`
  Upcoming change: in future versions (not this version) recipes will no longer reside in the `data/recipes` folder in datapacks; 
  instead they'll be in a dedicated directory along the lines of `data/anvil_crushing_recipes`. This is because anvil recipes 
  have so little in common with standard item recipes that it's become un-helpful and wasteful to use the vanilla recipe system. 
- 0.8.2 (17 Sep. 2020): Marked as compatible with 1.16.3, addressing [#6](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/issues/6)
- 0.8.1 (7 Sep. 2020): Fix [#5 Mobs drop no exp and no items](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/issues/5)
- 0.8.0 (6 Sep. 2020): New feature: entity crushing! 'Crushed' entities will be instantly killed. 
  - You can now specify `entity_ingredient` instead of `block_ingredient` in your recipes. `entity_ingredient`s may contain both entity entries and entity tags, similar to `block_ingredients`. 
  - **Important Change** to datapack format: `block_ingredient_drop_chance` is now `ingredient_drop_chance` to reflect that it will apply to either `block_ingredient` or `entity_ingredient`, whichever is specified. 
    If you have custom datapacks that used `block_ingredient_drop_chance`, you will need to update them to the new format. The 'Default Anvil Crushing Recipes' datapack has been updated. 
  - If `ingredient_drop_chance` is specified for an entity, it represents the chance that the entity will drop loot as though killed normally (it does not guarantee all possible drops will drop, they will have their normal random chances). 
    `ingredient_drop_chance` will only have affect mobs (`LivingEntity` internally), as they're the only entities that can have loot tables. 
  - New issues were likely introduced as a result of the
   -internal changes, bug reports will be appreciated. 