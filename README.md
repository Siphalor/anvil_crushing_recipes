Minecraft 1.16 mod for the [Fabric mod loader](https://www.fabricmc.net/). 

Download the mod on [CurseForge](https://www.curseforge.com/minecraft/mc-mods/anvil-crushing-recipes)!

This mod is in BETA. I have plans for more features, datapack format may change, and the mod  has not been tested on servers. Bug reports and feedback are welcome. 

 

Adds a new recipe type: anvil_crushing_recipes:anvil_crushing. Recipes can be added with datapacks.

Recipes take one or more blocks, entities, or tags as input and can produce a block and/or a list of items as output.

![Match State](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/raw/master/media/Anvil%20Crushing%20Recipes%20-%20Match%20State%20-%20Cropped.gif)
![Crush Entity](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/raw/master/media/Anvil%20Crushing%20Recipes%20-%20Entity%20Recipe%20-%20Cropped.gif) 

I've created a 'default' recipes datapack with a lot the obvious recipes in it (concrete to powder, stone to cobble, etc. ). You can find the latest version of the datapack by clicking on the file for the latest version of the mod on CurseForge and scrolling down to 'Additional Files'. You can also follow [this link](https://www.curseforge.com/minecraft/mc-mods/anvil-crushing-recipes/files/3016918), which I'll try to keep updated to the latest version. 

 There are some demonstration video at [this link](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/tree/master/media). 

Both basic and detailed information on datapack format can be found on [the wiki](https://gitlab.com/supersaiyansubtlety/anvil_crushing_recipes/-/wikis/home). There are also lots of good examples in of the basic datapack in the default recipes datapack. The format is as 'vanilla-like' as possible. 

This mod is only for Fabric and I won't be porting it to Forge. The license is [MIT](https://will-lucic.mit-license.org/), however, so anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
